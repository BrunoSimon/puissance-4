# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 12:56:02 2021

@author: Bruno
"""
# Import des noms du module
import tkinter as tkr
import random


def recap():
    print("nb lignes:", lignes.get(), ", nb colonnes:", colonnes.get())
    print("player 1:", name1.get(), (",niveau: " + str(IANiveau1.get()))
          if name1.get() == 'IA' else "")
    print("player 2:", name2.get(), (",niveau: " + str(IANiveau2.get()))
          if name2.get() == 'IA' else "")
    fenetre.destroy()


fenetre = tkr.Tk()
title = "Puissance 4"

fenetre.title(title + ": configuration")
fenetre.minsize(300, 200)

cadre = tkr.Frame(fenetre)
cadre.pack()
row = 0
tkr.Label(cadre, text="Nom du joueur 1").grid(row=row)
name1 = tkr.StringVar()
name1.set("IA")
tkr.Entry(cadre, textvariable=name1, width=20).grid(row=row, column=1)
IANiveau1 = tkr.IntVar()
IANiveau1.set(0)
row += 1
tkr.Label(cadre, text="niveau IA").grid(row=row, column=0)
radioF1 = tkr.Frame(cadre)
radioF1.grid(row=row, column=1)

for i in range(3):
    b = tkr.Radiobutton(radioF1, variable=IANiveau1,
                        text=str(i), value=i)
    b.pack(side='left', expand=1)

row += 1
tkr.Label(cadre, text="Nom du joueur 2").grid(row=row)
name2 = tkr.StringVar()
name2.set("IA")
tkr.Entry(cadre, textvariable=name2, width=20).grid(row=row, column=1)
IANiveau2 = tkr.IntVar()
IANiveau2.set(0)
row += 1
tkr.Label(cadre, text="niveau IA").grid(row=row, column=0)
radioF2 = tkr.Frame(cadre)
radioF2.grid(row=row, column=1)
for i in range(3):
    b = tkr.Radiobutton(radioF2, variable=IANiveau2,
                        text=str(i), value=i)
    b.pack(side='left', expand=1)

row += 1
tkr.Label(cadre, text="Nombre de colonnes").grid(row=row)
colonnes = tkr.IntVar()
colonnes.set(6)
tkr.Entry(cadre, textvariable=colonnes,).grid(row=row, column=1)
row += 1
tkr.Label(cadre, text="Nombre de lignes").grid(row=row)
lignes = tkr.IntVar()
lignes.set(5)
tkr.Entry(cadre, textvariable=lignes).grid(row=row, column=1)

bouton = tkr.Button(fenetre, text="Demarrer", command=recap)
bouton.pack()

fenetre.mainloop()


fen = tkr.Tk()
fen.focus_force()
fen.title(title)
nb_colonnes = min(colonnes.get(), 15)
nb_lignes = min(lignes.get(), 10)
xmarge = 5
ymarge = 5

NB_JOUEURS = int(2)  # Nombre de joueurs dans une partie
VIDE = 0
ROUGE = 1
JAUNE = 2  # type case grille (et couleur des joueurs)
# tuple correspondant aux 3 couleurs ci-dessus
SYMBOLE_COULEUR = (" ", "X", "0")
MAP_COULEUR = ("", "red", "yellow")
dic_couleur = {"red": 1, "yellow": 2}
NB_ALIGNES_GAGNANT = 4
taille_case = 60

rayon = taille_case // 2 - 5
diam_cercle = 2*rayon

canvas = tkr.Canvas(fen, bg='white', width=nb_colonnes * taille_case + xmarge*2,
                    height=(nb_lignes+1) * taille_case + ymarge * 2)
canvas.pack()
fen.update()


def grille(canvas):
    horizontale = 0
    while horizontale <= nb_lignes+1:
        canvas.create_line(xmarge, taille_case * horizontale, taille_case *
                           nb_colonnes+xmarge, taille_case * horizontale, fill='black')
        horizontale += 1

    verticale = 0
    while verticale <= nb_colonnes:
        canvas.create_line(taille_case * verticale+xmarge, taille_case,  taille_case *
                           verticale+xmarge, taille_case * (nb_lignes+1), fill='black')
        verticale += 1


def place_pion(canvas, ligne, colonne, couleur):
    x0 = taille_case * (colonne - 1) + 2 * xmarge
    # ligne inversé...
    y0 = taille_case * (nb_lignes + 1) - (taille_case * (ligne) - ymarge)
    x1 = x0 + diam_cercle
    y1 = y0 + diam_cercle
    return canvas.create_oval(x0, y0, x1, y1, fill=couleur, outline=couleur, width=5)


class DropPion:
    def __init__(self, canvas, couleur, listCol):
        self.canvas = canvas
        self.listCol = listCol
        self.iCol = 0
        self.couleur = couleur
        self.id = place_pion(canvas, nb_lignes+1, listCol[0], couleur)
        self.canvas.bind_all('<KeyPress-Left>', self.move_left)
        self.canvas.bind_all('<KeyPress-Right>', self.move_right)
        self.canvas.bind_all('<KeyPress-Down>', self.drop)

    def draw(self):
        self.canvas.delete(self.id)
        self.id = place_pion(canvas, nb_lignes+1,
                             self.listCol[self.iCol], self.couleur)

    def move_right(self, evt):
        self.iCol += 1
        self.iCol %= len(self.listCol)
        self.draw()

    def move_left(self, evt):
        self.iCol -= 1
        self.iCol %= len(self.listCol)
        self.draw()

    def drop(self, evt):
        coupCol = self.listCol[self.iCol]
        if (P4.joueurs[dic_couleur[self.couleur]-1].nom == "IA"):
            coupCol = int(choix_colonne(P4, dic_couleur[self.couleur]-1))

        ligne = lacher(P4, coupCol, dic_couleur[self.couleur])

        place_pion(self.canvas, ligne, coupCol, self.couleur)
        if coup_a_gagne(P4, ligne, coupCol):
            print("Felicition",
                  P4.joueurs[dic_couleur[self.couleur]-1].nom, " ", self.couleur)
            self.canvas.delete(self.id)
            popup(
                "Congrat " + P4.joueurs[dic_couleur[self.couleur]-1].nom + ", couleur:  " + self.couleur)
            fen.destroy()

        if not grille_pas_remplie(P4):
            popup("Match nul, plus de place")
            fen.destroy()

        self.couleur = "yellow" if self.couleur == "red" else "red"

        self.listCol = colonnesPossible(P4)
        self.draw()
        afficher_grille(P4)

        if (P4.joueurs[dic_couleur[self.couleur]-1].nom == "IA"):
            coupCol = int(choix_colonne(P4, dic_couleur[self.couleur]-1))
            ligne = lacher(P4, coupCol, dic_couleur[self.couleur])
            place_pion(self.canvas, ligne, coupCol, self.couleur)
            if coup_a_gagne(P4, ligne, coupCol):
                print("Felicition",
                  P4.joueurs[dic_couleur[self.couleur]-1].nom, " ", self.couleur)
                self.canvas.delete(self.id)
                popup(
                "Congrat " + P4.joueurs[dic_couleur[self.couleur]-1].nom + ", couleur:  " + self.couleur)
                fen.destroy()

            if not grille_pas_remplie(P4):
                popup("Match nul, plus de place")
                fen.destroy()

            self.couleur = "yellow" if self.couleur == "red" else "red"

            self.listCol = colonnesPossible(P4)
            self.draw()
            afficher_grille(P4)



def popup(txt):
    fInfos = tkr.Toplevel()		  # Popup -> Toplevel()
    fInfos.title('félicitation')
    label = tkr.Label(fInfos, text=txt)
    label.pack()
    tkr.Button(fInfos, text='Quitter',
               command=fInfos.destroy).pack(padx=10, pady=10)
    fInfos.transient(fen) 	  # Réduction popup impossible
    fInfos.grab_set()		  # Interaction avec fenetre jeu impossible
    fen.wait_window(fInfos)   # Arrêt script principal
# Définition d’un joueur


class Joueur:
    def __init__(self, inCouleur, inNom="", inNiveauIA=0):
        """
        ROLE : initialise un nouveau joueur avec les paramètres indiqués
        ENTREE inCouleur : ROUGE ou JAUNE
        inNom : str # nom du joueur (si vide, c’est l’ordinateur)
        SORTIE self : Joueur
        """
        self.couleur = int(inCouleur)  # En fait, soit ROUGE, soit JAUNE
        self.nom = str(inNom)  # Le nom du joueur
        self.ordinateur = bool(inNom == "")  # Est-ce l’ordinateur ?
        if inNom == "":
            self.nom = "IA"
        self.niveauIA = inNiveauIA
        self.nb_victoires = int(0)  # Nombre de victoires

# Définition des composants du jeu


class Jeu:
    def __init__(self, inNomJoueur1, inNomJoueur2="", inNiveauIA1=0, inNiveauIA2=0):
        """
        ROLE : initialise la grille de jeu et les joueurs
        ENTREE inNomJoueur1, inNomJoueur2 : str (si vide, c’est l’ordinateur)
        SORTIE self : Jeu
        """
        # 1.grille de jeu avec une bordure supplémentaire sur les 4 bords
        self.grille = []  # liste vide => on va lui rajouter les lignes
        for ligne in range(nb_lignes+2):  # 2 lignes de plus pour les bords
            self.grille.append(list((nb_colonnes+2)*[0]))  # 2 colonnes en +
        # 2.les 2 joueurs
        Joueur1 = Joueur(ROUGE, inNomJoueur1, inNiveauIA1)
        Joueur2 = Joueur(JAUNE, inNomJoueur2, inNiveauIA2)
        self.joueurs = (Joueur1, Joueur2)


def vider_grille(ioJeu):
    """
    ROLE : vide la grille du jeu. Ceci correspond à actionner
    la barre située sous la grille du jeu réel.
    MAJ ioJeu : Jeu # on vide la grille mais on conserve les joueurs !
    """
    i = int()
    j = int()  # indices pour parcourir lignes et colonnes
    for i in range(nb_lignes + 2):  # vider ligne i
        for j in range(nb_colonnes + 2):  # vide case i,j
            ioJeu.grille[i][j] = VIDE


def symbole_case(inCouleur):  # str
    """
    ROLE : renvoie le caractère symbolisant la valeur d’une case
    (ce symbole est défini dans la constante-tupe SYMBOLE_COULEUR)
    ENTREE inCouleur : VIDE ou ROUGE ou JAUNE
    """
    return SYMBOLE_COULEUR[inCouleur]


def afficher_numeros_colonnes():
    """
    ROLE : affiche une ligne correspondant au bord supérieur ou inférieur
    en indiquant les numéros des colonnes
    """
    j = int()  # index pour parcourir les colonnes de la grille
    print(" ", end=" ")  # espace pour le bord droit
    for j in range(1, nb_colonnes+1):
        print(j, end=" ")
    print()  # pour aller à la Ligne


def afficher_grille(inJeu):
    """
    ROLE : affiche la grille du jeu et son contenu
    ENTREE inJeu : Jeu
    """
    i = int()
    j = int()  # index pour parcourir les cases de la grille
    # 1.afficher le titre
    print()  # sauter une ligne
    print("  ", title)  # ajout de 2 espaces pour centrer le titre sur la grille
    # 2.afficher le bord supérieur
    afficher_numeros_colonnes()
    # 3.afficher la grille avec les bords gauche et droit
    for i in range(nb_lignes, 0, -1):  # on affiche d’abord la ligne supérieure
        # 3.1.afficher le numéro de ligne à gauche
        print(i, end=" ")
        # 3.2.afficher la ligne i
        for j in range(1, nb_colonnes+1):
            print(symbole_case(inJeu.grille[i][j]), end=" ")
        # 3.3.afficher le numéro de ligne à droite et passer à la ligne
        print(i)
    # 4.afficher le bord inférieur
    afficher_numeros_colonnes()


def est_coup_possible(inJeu, inColonne):  # return bool
    """
    ROLE : renvoie vrai si la colonne spécifiée peut être jouée,
    c’est-à-dire si elle n’est pas encore pleine
    ENTREE inJeu : Jeu ; inColonne : int
    """
    if inColonne < 1 or inColonne > nb_colonnes:  # on est hors de la grille !
        return False
    else:
        return inJeu.grille[nb_lignes][inColonne] == VIDE  # test ligne du haut
        # rappel : numéros de ligne entre 0 (bord bas) à nb_lignes+1 (bord haut)


def ligne_de_chute(inJeu, inColonne):
    """
    ROLE : renvoie la ligne où tombe un jeton lâché à la colonne spécifiée
    HYPOTHESE : on suppose la colonne non pleine
    ENTREE inJeu : Jeu ; inColonne : int
    """
    ligne = int()  # index pour parcourir les lignes de la colonne considérée
    # 1.on recherche la 1ere ligne vide. On sait qu’il y en a une par hypothèse
    ligne = 1
    while inJeu.grille[ligne][inColonne] != VIDE:
        ligne = ligne + 1
    return ligne


def lacher(ioJeu, inColonne, inCouleur):
    """
    ROLE : lache sur la grille à la colonne indiquée un jeton de couleur donnée
    HYPOTHESE : on suppose la colonne non pleine
    ENTREE inColonne : int ; inCouleur : ROUGE ou JAUNE
    M.A J. ioJeu : Jeu # la grille contiendra un pion de plus
    """
    ligne = ligne_de_chute(ioJeu, inColonne)
    ioJeu.grille[ligne][inColonne] = inCouleur
    return ligne


def nb_pions_dir(inJeu, inLigne, inColonne, inDirX, inDirY):  # return int
    """
    ROLE : renvoie le plus grand nombre de jetons de la même couleur alignés
    suivant la direction (inDirX, inDirY) et incluant le jeton
    de la case (inLigne,inColonne)
    HYPOTHESES : la position spécifiée par inLigne, inColonne contient bien
    un jeton et au moins une des 2 directions n’est pas nulle.
    ENTREE inJeu : Jeu ;
    inLigne, inColonne : int # position du jeton testé
    inDirX, inDirY : int # direction (1,1), (1,0), (1,-1) ou (0,1)
    """
    lig = int()  # ligne dans la direction +/- inDirY
    col = int()  # colonne dans la direction +/- inDirX
    couleur = int()  # la couleur de l’alignement
    nbPions = int()  # nombre de pions alignés en cours de calcul

    # 1.initialiser
    couleur = inJeu.grille[inLigne][inColonne]
    nbPions = 1  # le jeton situé en (inLigne, inColonne)

    # 2.comptabiliser les jetons dans la direction (inDirX,inDirY)
    lig = inLigne + inDirY
    col = inColonne + inDirX
    while inJeu.grille[lig][col] == couleur:  # boucle finie car bords vides
        nbPions = nbPions + 1
        lig = lig + inDirY
        col = col + inDirX

    # 3.Comptabiliser les jetons dans la direction opposée (-inDirX,-inDirY)
    lig = inLigne - inDirY
    col = inColonne - inDirX
    while inJeu.grille[lig][col] == couleur:  # boucle finie car bords vides
        nbPions = nbPions + 1
        lig = lig - inDirY
        col = col - inDirX

    # 4.renvoi du résultat
    # print("("+str(inDirX)+","+str(inDirY)+"):", nbPions) # affichage-test
    return nbPions

# def max(inEntier1, inEntier2) : # return int
# 	"""
# 	ROLE : renvoie la plus grande des 2 valeurs spécifiées
# 	ENTREE inEntier1,inEntier2 : int
# 	"""
# 	if inEntier1 > inEntier2 : return inEntier1
# 	else : return inEntier2


def nb_pions_alignes(inJeu, inLigne, inColonne):  # return int
    """
    ROLE : renvoie le plus grand nombre de jetons de la même couleur alignés
    horizontalement, verticalement ou en diagonale et incluant le jeton
    de la position spécifiée (inLigne, inColonne)
    ENTREE inJeu : Jeu ; inLigne, inColonne : int
    """
    nbPions = int(1)  # nombre de pions alignés
    list_direction = [(1, 1), (1, 0), (1, -1), (0, 1)]
    for direc in list_direction:
        nbPions = max(nbPions, nb_pions_dir(
            inJeu, inLigne, inColonne, direc[0], direc[1]))
        # if nbPions >= NB_ALIGNES_GAGNANT :
        #  return nbPions
    return nbPions


def coup_a_gagne(inJeu, ligne, colonne):
    """
    ROLE : renvoie suffisament de pions sont alignés
    ENTREE True or False
    """
    return nb_pions_alignes(inJeu, ligne, colonne) >= NB_ALIGNES_GAGNANT


def change_de_joueur(numeroJoueur):
    return (numeroJoueur + 1) % 2


def alerte_coup_impossible(coupCol):
    """
    ROLE:  Affiche un message d'avertissement'
    ENTREE: coupCol
    """
    print("Impossible de jouer la colonne", coupCol)
    print("Essaye encore")


def grille_pas_remplie(inJeu):
    """
    ROLE:  test si la grille n'est pas remplie'
    ENTREE: inJeu: Jeu
    """
    for j in range(1, nb_colonnes+1):
        if inJeu.grille[nb_lignes+1][j] == VIDE:
            return True
    return False


def choix_colonne(inJeu, numjoueur):
    """
    ROLE:  permet de choisir le prochain mouvement en mode text
    Sortie:  col : string
    """
    if inJeu.joueurs[numjoueur].nom == "IA":
        if inJeu.joueurs[numjoueur].niveauIA == 0:
            choix = str(colonne_conseillee_aleatoire(inJeu))
        elif inJeu.joueurs[numjoueur].niveauIA == 1:
            choix = str(colonne_conseillee(
                inJeu, inJeu.joueurs[numjoueur].couleur))
        else:
            choix = str(colonne_conseillee_strategique(
                inJeu, inJeu.joueurs[numjoueur].couleur))
    else:
        print("Ecrire stop pour arreter la partie, reset pour recommencer.")
        choix = input("Entrez le numéro de column entre 1 et " +
                      str(nb_colonnes) + " : ")
    return choix


def colonnesPossible(inJeu):
    tab_col_possibles = []
    for colonne in range(nb_colonnes+1):
        if est_coup_possible(inJeu, colonne):
            tab_col_possibles.append(colonne)
    return tab_col_possibles


def colonne_conseillee_aleatoire(inJeu):  # return int
    """
    ROLE : renvoie une colonne conseillée pour jouer le prochain coup
    PRINCIPE : colonne choisie au hasard entre les colonnes possibles !
    ENTREE inJeu
    """
    tab_col_possibles = colonnesPossible(inJeu)
    # print(tab_col_possibles)
    indice_col_aleatoire = random.randint(
        0, len(tab_col_possibles)-1)  # min inclu, max exclu
    colonne = tab_col_possibles[indice_col_aleatoire]
    return colonne


def colonne_conseillee(inJeu, inCouleur):  # return int
    """
    ROLE : renvoie une colonne conseillée pour jouer le prochain coup
    PRINCIPE : la colonne conseillée est celle qui permet :
    - en priorité d’empecher l’adversaire de gagner au coup suivant
    - sinon, d’obtenir l’alignement le plus long.
    ENTREE inJeu : Jeu
    inCouleur : ROUGE ou JAUNE # couleur du joueur qui va jouer
    """
    col = int()  # colonne en cours de test
    nbPions = int()  # nombre de pions alignés pour colonne col
    maxPions = int()  # nombre maximal de pions alignés
    ligneATester = int()  # ligne de chute de la colonne col
    couleurAdverse = int()  # couleur de l’adversaire
    coupGagnant = bool()  # vrai si l’on gagne

    coupAParer = bool()  # vrai si l’on doit parer un coup de l’adversaire
    # lui permettant de gagner au coup suivant

    couleurAdverse = NB_JOUEURS-inCouleur+1  # si 2, c’est 1 ; si 1, c’est 2
    coupAParer = False  # a priori
    maxPions = 0  # nécessairement un minorant strict
    col = 1  # première colonne à tester
    while not coupGagnant and not coupAParer and col <= nb_colonnes:
        # déterminer la ligne de chute
        if est_coup_possible(inJeu, col):
            ligneATester = ligne_de_chute(inJeu, col)

            # simuler si on gagne
            inJeu.grille[ligneATester][col] = inCouleur
            if nb_pions_alignes(inJeu, ligneATester, col) >= NB_ALIGNES_GAGNANT:
                colonneAConseiller = col
                coupGagnant = True  # coup à jouer pour gagner!

            # simuler la pose du pion adverse pour voir si ça le fait gagner
            inJeu.grille[ligneATester][col] = couleurAdverse
            if nb_pions_alignes(inJeu, ligneATester, col) >= NB_ALIGNES_GAGNANT:
                colonneAConseiller = col
                coupAParer = True  # coup à parer d’urgence !
            else:
                # simuler la pose de son propre pion
                inJeu.grille[ligneATester][col] = inCouleur
                # évaluer le coup
                nbPions = nb_pions_alignes(inJeu, ligneATester, col)
                # choisir le coup (par rapport au meilleur précédent)
                if (nbPions > maxPions) or (nbPions == maxPions and random.randint(0, 1) > 0.5):
                    maxPions = nbPions
                    colonneAConseiller = col
            # retirer le coup testé
            inJeu.grille[ligneATester][col] = VIDE
        # passer à la colonne suivante
        col = col + 1
    return colonneAConseiller


def colonne_conseillee_strategique(inJeu, inCouleur):  # return int
    """
    ROLE : renvoie une colonne conseillée pour jouer le prochain coup
    PRINCIPE : la colonne conseillée est celle qui permet :
    - en priorité d’empecher l’adversaire de gagner au coup suivant
    - sinon, d’obtenir l’alignement le plus long.
    ENTREE inJeu : Jeu
    inCouleur : ROUGE ou JAUNE # couleur du joueur qui va jouer
    """
    col = int()  # colonne en cours de test
    nbPions = int()  # nombre de pions alignés pour colonne col
    maxPions = int()  # nombre maximal de pions alignés
    ligneATester = int()  # ligne de chute de la colonne col
    couleurAdverse = int()  # couleur de l’adversaire
    coupGagnant = bool()  # vrai si l’on gagne

    coupAParer = bool()  # vrai si l’on doit parer un coup de l’adversaire
    # lui permettant de gagner au coup suivant

    tab_coups_optimaux = []  # liste des meilleurs coups de l'ia
    # liste des colonnes qui font gagner l'adversaire au coup suivant
    tab_coups_perdants = []
    couleurAdverse = NB_JOUEURS-inCouleur+1  # si 2, c’est 1 ; si 1, c’est 2

    # teste si l'adversaire peut gagner au coup suivant en remplissant tab_coups_perdants
    col = 1
    while col <= nb_colonnes:
        inJeu.grille[ligneATester][col] = inCouleur
        # si l'adversaire peut gagner au coup suivant on met

        col_test = 1
        col_test_adv = 1
        while col_test <= nb_colonnes:
            if est_coup_possible(inJeu, col_test):
                ligneATester = ligne_de_chute(inJeu, col_test)
                inJeu.grille[ligneATester][col_test] = inCouleur
                coupPerdant = False
                while col_test_adv <= nb_colonnes:
                    # print(col_test_adv)
                    if est_coup_possible(inJeu, col_test_adv):
                        ligne_de_chute_adv = ligne_de_chute(
                            inJeu, col_test_adv)
                        inJeu.grille[ligne_de_chute_adv][col_test_adv] = couleurAdverse
                        if coup_a_gagne(inJeu, ligne_de_chute_adv, col_test_adv):
                            tab_coups_perdants.append(col_test)
                        inJeu.grille[ligne_de_chute_adv][col_test_adv] = VIDE
                        print("??")

                    col_test_adv = col_test_adv + 1
                inJeu.grille[ligneATester][col_test] = VIDE
            col_test = col_test + 1
        col += 1
    coupAParer = False  # a priori
    coupPerdant = False  # a priori

    maxPions = 0  # nécessairement un minorant strict
    col = 1  # première colonne à tester
    while not coupGagnant and not coupAParer and not coupPerdant and col not in tab_coups_perdants and col not in tab_coups_optimaux and col <= nb_colonnes:
        # déterminer la ligne de chute
        if est_coup_possible(inJeu, col):
            ligneATester = ligne_de_chute(inJeu, col)

            # simuler si on gagne
            inJeu.grille[ligneATester][col] = inCouleur
            if nb_pions_alignes(inJeu, ligneATester, col) >= NB_ALIGNES_GAGNANT:
                colonneAConseiller = col
                coupGagnant = True  # coup à jouer pour gagner!

            # simuler la pose du pion adverse pour voir si ça le fait gagner
            inJeu.grille[ligneATester][col] = couleurAdverse
            if nb_pions_alignes(inJeu, ligneATester, col) >= NB_ALIGNES_GAGNANT:
                colonneAConseiller = col
                coupAParer = True  # coup à parer d’urgence !
            else:
                if col in tab_coups_perdants:
                    coupPerdant = True
    # tab_menaces
                # simuler la pose de son propre pion
                inJeu.grille[ligneATester][col] = inCouleur
                # évaluer le coup
                nbPions = nb_pions_alignes(inJeu, ligneATester, col)
                # choisir le coup (par rapport au meilleur précédent)
                if (nbPions > maxPions) or (nbPions == maxPions and random.randint(0, 1) > 0.5):
                    maxPions = nbPions
                    colonneAConseiller = col
            # retirer le coup testé
            inJeu.grille[ligneATester][col] = VIDE
        # passer à la colonne suivante
        col = col + 1
    return colonneAConseiller


grille(canvas)


def stopGame():
    fen.destroy()


bouton = tkr.Button(fen, text="stop", command=stopGame)
bouton.pack()

currentJoueur = 0
P4 = Jeu(name1.get(), name2.get(), IANiveau1.get(), IANiveau2.get())
afficher_grille(P4)

dp = DropPion(canvas, 'red', colonnesPossible(P4))
dp.draw()


fen.mainloop()
