Durée 8h.  
Équipes de 3 (inconnue avant le challenge, compétences différentes).  
Objectif :  
- Réaliser le moteur de jeu
- Interface graphique
- 3 niveaux d'IA  

Réalisation dans le temps imparti (main.py) :  
- Développement en Python 3  
- Moteur de jeu  
- Interface en console, début d'interface graphique avec tkinter  
- 2 niveaux d'IA  

Finalisation de l'interface graphique aprés le challenge (mainGUI.py)
